const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
loadColorList = () => {
  let contentHTML = `
    <button class="color-button pallet"></button>
    <button class="color-button viridian"></button>
    <button class="color-button pewter"></button>
    <button class="color-button cerulean"></button>
    <button class="color-button vermillion"></button>
    <button class="color-button lavender"></button>
    <button class="color-button celadon"></button>
    <button class="color-button saffron"></button>
    <button class="color-button fuschia"></button>
    <button class="color-button cinnabar"></button>
    `;
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
loadColorList();
document.querySelector(".pallet").addEventListener("click", () => {
  document.getElementById("house").className = "house pallet";
});
document.querySelector(".viridian").addEventListener("click", () => {
  document.getElementById("house").className = "house viridian";
});
document.querySelector(".pewter").addEventListener("click", () => {
  document.getElementById("house").className = "house pewter";
});
document.querySelector(".cerulean").addEventListener("click", () => {
  document.getElementById("house").className = "house cerulean";
});
document.querySelector(".vermillion").addEventListener("click", () => {
  document.getElementById("house").className = "house vermillion";
});
document.querySelector(".lavender").addEventListener("click", () => {
  document.getElementById("house").className = "house lavender";
});
document.querySelector(".celadon").addEventListener("click", () => {
  document.getElementById("house").className = "house celadon";
});
document.querySelector(".saffron").addEventListener("click", () => {
  document.getElementById("house").className = "house saffron";
});
document.querySelector(".cinnabar").addEventListener("click", () => {
  document.getElementById("house").className = "house cinnabar";
});
document.querySelector(".fuschia").addEventListener("click", () => {
  document.getElementById("house").className = "house fuschia";
});
