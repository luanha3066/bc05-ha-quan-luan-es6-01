tinhDiemTB = (...monHoc) => {
  let sum = 0;
  monHoc.forEach((item) => {
    sum += item;
  });
  let diemTB = sum / monHoc.length;
  return diemTB;
};
document.getElementById("btnKhoi1").addEventListener("click", () => {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  document.getElementById("tbKhoi1").innerHTML = tinhDiemTB(
    diemToan,
    diemLy,
    diemHoa
  ).toFixed(1);
});
document.getElementById("btnKhoi2").addEventListener("click", () => {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemAnh = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerHTML = tinhDiemTB(
    diemVan,
    diemSu,
    diemDia,
    diemAnh
  ).toFixed(1);
});
